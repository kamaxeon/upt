"""Output loggers."""
from cki_lib.logger import file_logger


class COLORS:
    # pylint: disable=too-few-public-methods
    """Escape codes to colarize terminal output."""

    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m'


LOGGER = file_logger(__name__, dst_file='info.log')


def colorize(data):
    """Colorize specific keywords using pre-defined colors."""
    color_table = {COLORS.YELLOW: ['Warn', 'Aborted', 'watchdog!'],
                   COLORS.GREEN: ['Pass', 'Running', 'done'],
                   COLORS.MAGENTA: ['Skip'],
                   COLORS.RED: ['FAIL', 'ERROR', 'Panic']}
    for word in data.split(' '):
        for color, keywords in color_table.items():
            for keyword in keywords:
                if keyword.lower() == word.lower():
                    data = data.replace(word, f'{color}{word}{COLORS.RESET}', 1)
                    break

    return data


def print_result_in_color(data):
    """Colorize specific keywords using pre-defined colors and print it."""
    print(colorize(data))


def printc(data, color=COLORS.BLUE):
    """Colorize data using a single color and print it."""
    print(f'{color}{data}{COLORS.RESET}')


LOGGER.printc = printc
LOGGER.print_result_in_color = print_result_in_color
