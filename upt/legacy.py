"""Legacy commands."""
import os
import pathlib

from bs4 import BeautifulSoup as BS
from cki_lib.logger import logger_add_fhandler
import click
from ruamel.yaml.scalarstring import PreservedScalarString

from plumbing.format import ProvisionData
from plumbing.objects import Host
from plumbing.objects import RecipeSet
from plumbing.objects import ResourceGroup
from upt import const
from upt import misc
from upt.logger import LOGGER


def adjust_job_element(job, job_group):
    """Keep, set or remove group attribute in job element."""
    if job_group == '':
        del job['group']
    elif job_group is not None:
        job['group'] = job_group


def init_resource_group(xml, i, **kwargs):
    """Create a new resource group for each recipeSet.

    Restraint client has a nasty habit of exiting on EWD, even though that affects only 1 recipeSet, not the whole job.
    Because of this, each resource_group currently has only 1 recipeSet.
    """
    resource_group = ResourceGroup()
    soup = BS(xml, 'xml')
    job = soup.find('job')

    adjust_job_element(job, kwargs.get('group'))

    # make job soup empty to create a template
    for child in job.find_all("recipeSet"):
        child.decompose()

    # save this in yaml in pretty format
    job_content = str(job).replace('\n', '').strip()
    job_content = misc.get_whiteboard(job_content, i, is_preprovisioned=False)
    resource_group.job = PreservedScalarString(job_content)

    return resource_group


def _delete_aborted_recipes(soup):
    """Remove aborted recipes/recipesets from soup."""
    for recipe in soup.find_all('recipe'):
        if misc.recipe_not_provisioned(recipe):
            recipe.decompose()

    for recipeset in soup.find_all('recipeSet'):
        if not recipeset.find_all('recipe'):
            recipeset.decompose()


def append_recipeset(xml, bkr, recipeset, i, **kwargs):
    """Add a recipeset to Beaker provisioner."""
    resource_group = init_resource_group(xml, i, **kwargs)
    bkr.rgs.append(resource_group)

    alt_soup = BS(const.JOB_XML_STUB, 'xml')
    restraint_soup = alt_soup.find('recipeSet')

    new_recipeset = RecipeSet()

    for recipe in recipeset.find_all('recipe'):
        new_recipe = BS('<recipe />', 'xml').find('recipe')
        for task in recipe.find_all('task'):
            new_recipe.append(task)

        restraint_soup.append(new_recipe)

        new_host = Host({'hostname': '', 'recipe_id': None, 'recipe_fill': ''})
        # Duration force by cmd-line args takes precendece. Otherwise use
        # 120% of added-up task KILLTIMEOVERRIDE. If that's still 0, use
        # 8hours.
        host_tasks_duration = misc.compute_recipe_duration(new_recipe)
        force_host_duration = kwargs['force_host_duration']
        new_host.duration = force_host_duration if force_host_duration \
            else int(host_tasks_duration / 100 * 120 if host_tasks_duration
                     else const.DEFAULT_RESERVESYS_DURATION)

        # override recipe_fill
        new_host.recipe_fill = PreservedScalarString(
            BS(str(recipe), 'xml').find('recipe'))
        new_recipeset.hosts.append(new_host)

    # remove and warn about tasks without fetch element; restraint cannot
    # run these
    misc.fixup_or_delete_tasks_without_fetch(alt_soup)
    # convert: write restraint xml
    new_recipeset.restraint_xml = PreservedScalarString(
        alt_soup.prettify())
    resource_group.recipeset = new_recipeset


def convert_xml(xml, **kwargs):
    """Convert Beaker XML file into a provisioning request."""
    # Create main data object, this will serialize into converted provisioning
    # request (c_req.yaml).
    prov_data = ProvisionData()

    # create the soup a-new, because the previous was decomposed
    soup = BS(xml, 'xml')
    _delete_aborted_recipes(soup)

    soup_recipesets = soup.find_all('recipeSet')
    if not soup_recipesets:
        return prov_data

    provisioner = prov_data.get_provisioner(kwargs['provisioner'], create=True)
    # Go through all recipesets in original XML and split them to resource groups.
    for i, recipeset in enumerate(soup_recipesets, start=1):
        append_recipeset(xml, provisioner, recipeset, i, **kwargs)

    # Make sure we don't run on certain hosts.
    excluded_hosts_path = pathlib.Path(kwargs.get('excluded_hosts'))
    if excluded_hosts_path.is_file():
        hostnames = excluded_hosts_path.read_text()
        prov_data.get_provisioner(kwargs['provisioner']).exclude_hosts(hostnames.splitlines())

    return prov_data


@click.group()
@click.pass_context
def legacy(ctx, **kwargs):
    # pylint: disable=unused-argument
    """Commands for legacy pipeline."""
    # Put log file info.log into current directory.
    logger_add_fhandler(LOGGER, 'info.log', os.getcwd())


@legacy.command()
@click.option('-i', '--input_path', required=True,
              help='Path to beaker.xml to convert. Default: beaker.xml',
              default='beaker.xml')
@click.option('-r', '--request-file', required=True, default='c_req.yaml',
              help='Path to output (converted) file. Default: c_req.yaml.')
@click.option('-g', '--group', required=False, default=None,
              help='Optional. Beaker group override. When running using files'
                   ' from the pipeline (cki group), use "" to override group'
                   ' and submit jobs using your own account.')
@click.option('-p', '--provisioner', required=True, default='beaker',
              help='Which provisioner will be used with this request, so the '
                   'information is saved into the resulting yaml. Default: beaker')
@click.pass_context
def convert(ctx, **kwargs):
    """Convert standard kpet beaker.xml to UPT provisioning request."""
    # pack all arguments into kwargs
    kwargs.update(ctx.parent.parent.params)

    # read xml
    xml = pathlib.Path(kwargs['input_path']).read_text()

    print(f'Converting {kwargs["input_path"]} to {kwargs["request_file"]}...')

    provisioner_data = convert_xml(xml, **kwargs)
    # write everything to file
    provisioner_data.serialize2file(kwargs['request_file'])
