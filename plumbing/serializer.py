"""UPT provisioning request (de)serialization."""

from rcdefinition.rc_data import DefinitionBase


class DeserializerBase(DefinitionBase):
    """Yaml deserializer helper."""

    @classmethod
    def to_yaml(cls, representer, data):
        """Serialize yaml as a given class."""
        data2serialize = {}
        # Rebuild the dict; do not serialize attributes starting with '_'
        _ = [data2serialize.update({key: value}) for key, value in
             data.items() if not key.startswith('_')]

        return representer.represent_mapping(cls.__name__, data2serialize)
