"""AWS provisioner."""
import copy
from functools import cached_property
import json
import os
import shlex
import subprocess

import boto3
import botocore.exceptions
from bs4 import BeautifulSoup as BS
from cki_lib import misc
from ruamel.yaml.scalarstring import PreservedScalarString

from plumbing.interface import ProvisionerCore
from upt.const import EC2_INSTANCE_RUNNING
from upt.logger import LOGGER
from upt.misc import fixup_or_delete_tasks_without_fetch


class AWS(ProvisionerCore):
    """AWS provisioner."""

    session = boto3.Session()

    @cached_property
    def ec2(self):
        # pylint: disable=no-self-use
        """Get AWS's session ec2."""
        return AWS.session.resource('ec2')

    # pylint: disable=E1101, W0212
    def __init__(self, dict_data=None):
        """Construct instances after deserialization."""
        super().__init__(dict_data=dict_data)

    @property
    def aws_config(self):
        """Get aws configuration for UPT."""
        return json.loads(os.environ['AWS_UPT_CONFIG'])

    def hosts(self):
        """Iterate through all AWS hosts."""
        for resource_group in self.rgs:
            for host in resource_group.hosts():
                yield host

    def provision(self, **kwargs):
        """Provision all hosts in all resource groups."""
        self.keycheck = kwargs.get('keycheck')

        preprovision_arg = kwargs.get('preprovision')
        preprovision_indexes = [int(x) for x in preprovision_arg.split(',')] if preprovision_arg else []
        # Add pre-provisioned resource groups.
        self.rgs += [copy.deepcopy(self.rgs[idx]) for idx in preprovision_indexes]
        if not preprovision_indexes:
            LOGGER.debug('* No pre-provisioning enabled.')

        for resource_group in self.rgs:
            # We currently don't have a use for resource_id with AWS, so just use an increasing unique number.
            resource_group.resource_id = str(self.resource_id_monotonic.get())
            recipe_set = resource_group.recipeset
            soup = BS(recipe_set.restraint_xml, 'xml')
            recipes = soup.find_all('recipe')

            assert len(recipes) == len(recipe_set.hosts), "Invalid data; must have 1 host per 1 recipe"

            for j, host in enumerate(recipe_set.hosts):
                instance_id = host.misc.get('instance_id')
                if not instance_id:
                    host.recipe_id = self.host_recipe_id_monotonic.get()
                    host._instance = self.provision_host(host)[0]
                    host.misc['instance_id'] = host._instance.id
                else:
                    host._instance = self.ec2.Instance(instance_id)

                recipes[j]['id'] = host.recipe_id

            fixup_or_delete_tasks_without_fetch(soup)
            recipe_set.restraint_xml = PreservedScalarString(soup.prettify())

    def provision_host(self, host):
        """Provision a single host."""
        return self.ec2.create_instances(
            LaunchTemplate={'LaunchTemplateName': self.aws_config['launch_template']},
            TagSpecifications=[
                {
                    'ResourceType': 'instance',
                    'Tags': [{
                        'Key': 'Name',
                        'Value': f'{self.aws_config["instance_prefix"]}.{host.recipe_id}'
                    }]
                }
            ],
            MaxCount=1,
            MinCount=1,
        )

    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""
        provisioned, _ = self.get_provisioning_state(resource_group)
        return provisioned

    def get_provisioning_state(self, resource_group):
        """Get current state of provisioning.

        NOTE: this method is not a required part of the provisioner interface. Each provisioner implements and uses
        this method in its own way, although the method signature is very similar each time.
        """
        provisioned = True
        erred = []

        for host in resource_group.hosts():
            if not host._instance:
                host._instance = self.ec2.Instance(host.misc.get('instance_id'))
            try:
                host._instance.reload()
            # should be resolved by eventual consistency
            except botocore.exceptions.ClientError:
                provisioned = False
                LOGGER.debug('Waiting for host %s to be created', host._instance.id)
                continue

            # If for whatever reason the public_dns_name isn't available yet, wait, so we can do readiness check with
            # ssh down below.
            if not host._instance.public_dns_name:
                provisioned = False
                host._instance.reload()
                continue

            if host._instance.state['Code'] != EC2_INSTANCE_RUNNING:
                provisioned = False
                LOGGER.debug('Waiting for host %s to reach running state', host._instance.id)
            # We need to check if UserData startup script has finished, but only once.
            elif not host._user_data_done:
                cmd = f'ssh -o PubkeyAcceptedKeyTypes=+ssh-rsa' \
                      f' -o StrictHostKeyChecking={self.keycheck} fedora@{host._instance.public_dns_name}' \
                      f' [[ -e /var/lib/cloud/instance/boot-finished ]]'
                out, err, retcode = misc.safe_popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if err.strip():
                    LOGGER.debug(err)
                if out.strip():
                    print(out)
                if retcode:
                    LOGGER.debug('Waiting for host %s to finish UserData script', host._instance.id)
                    provisioned = False
                else:
                    host._user_data_done = True

            if host._instance.state['Code'] > EC2_INSTANCE_RUNNING:
                erred.append(host)

        return provisioned, erred

    def reprovision_aborted(self):
        """Provision a resource again, if provisioning failed."""
        for host in self.hosts():
            if host._instance.state['Code'] > EC2_INSTANCE_RUNNING:
                host._instance.terminate()
                del host._instance
                host._user_data_done = False
                # We just removed an instance that is somehow broken (hopefully equivalent of Beaker "aborted"), so
                # we will provision it again.
                host._instance = self.provision_host(host)

    def release_rg(self, resource_group):
        """Release all resources provisioned in a single resource group."""
        for host in resource_group.hosts():
            if not host._instance:
                host._instance = self.ec2.Instance(host.misc.get('instance_id'))
            host._instance.terminate()

    def heartbeat(self, resource_group, recipe_ids_dead):
        """
        Check if resource is OK (provisioned, not broken/aborted).

        Add all not OK recipe_id's to recipe_ids_dead
        """
        _, erred = self.get_provisioning_state(resource_group)
        # no op if empty set
        recipe_ids_dead |= {host.recipe_id for host in erred}
        # AWS has no concept of EWD, however, if the host becomes unresponsive, we can still apply the same actions
        # to it as if it suffered EWD.

    def update_provisioning_request(self, resource_group):
        """Ensure that request file has up-to-date info after provisioning.

        This method is to be called at the end, when we know we've successfully
        provisioned resources we need. This method will skip resources that
        failed provisioning.
        """
        for host in resource_group.hosts():
            host.hostname = host._instance.public_dns_name

    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""
        return [host.misc.get('instance_id') for host in self.hosts() if host.misc.get('instance_id')]
