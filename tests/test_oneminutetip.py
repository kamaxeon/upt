"""Test cases for 1minutetip provisioner module."""
import os
import unittest
from unittest import mock

from bs4 import BeautifulSoup as BS

from plumbing.format import ProvisionData
from plumbing.objects import Host
from provisioners.oneminutetip import OneMinuteTip
from tests.const import ASSETS_DIR


class TestOneMinuteTip(unittest.TestCase):
    # pylint: disable=no-member
    """Test cases for 1minutetip module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')
        self.prov = ProvisionData.deserialize_file(self.req_asset)
        # Use Beaker assets to populate 1minutetip instead.
        self.tip = OneMinuteTip(self.prov.get_provisioner('beaker').to_mapping())

    def test_get_distro_requires(self):
        """Ensure get_distro_requires works."""
        recipe1 = self.tip.rgs[0].recipeset.hosts[0].recipe_fill
        ret1 = self.tip.get_distro_requires(BS(recipe1, 'xml'))
        self.assertEqual('RHEL', ret1)

        with self.assertRaises(RuntimeError):
            recipe2 = self.tip.rgs[0].recipeset.hosts[1].recipe_fill
            self.tip.get_distro_requires(BS(recipe2, 'xml'))

        with self.assertRaises(RuntimeError):
            recipe3 = self.tip.rgs[0].recipeset.hosts[2].recipe_fill
            self.tip.get_distro_requires(BS(recipe3, 'xml'))

    @mock.patch('provisioners.oneminutetip.LOGGER.error')
    @mock.patch('provisioners.oneminutetip.safe_popen')
    @mock.patch('subprocess.PIPE')
    @mock.patch('provisioners.oneminutetip.OneMinuteTip.get_distro_requires', return_value='rhel8')
    def test_provision_host(self, _, mock_pipe, mock_safe_popen, mock_error):
        """Ensure provision_host works."""
        mock_safe_popen.return_value = ('', 'err', 1)
        with self.assertRaises(RuntimeError):
            host = Host()
            self.tip.provision_host(host)

        mock_safe_popen.assert_called_with([self.tip.wrapper_path, 'provision', 'rhel8'],
                                           stdout=mock_pipe, stderr=mock_pipe)
        mock_error.assert_called_with('err')

    @mock.patch('provisioners.oneminutetip.safe_popen')
    @mock.patch('provisioners.oneminutetip.OneMinuteTip.get_distro_requires', return_value='rhel8')
    def test_provision_host_success(self, _, mock_safe_popen):
        """Ensure provision_host can finish properly."""
        mock_safe_popen.return_value = ('1mt:10.0.0.1', '', 0)

        host = Host()
        self.tip.provision_host(host)
        self.assertEqual('1mt', host.misc['instance_id'])
        self.assertEqual('10.0.0.1', host.hostname)

    @mock.patch('provisioners.oneminutetip.safe_popen')
    def test_release_resources(self, mock_safe_popen):
        """Ensure release_resources works."""
        self.tip.release_resources()

        mock_safe_popen.assert_any_call([self.tip.wrapper_path, 'cancel', '123'])

    def test_get_resource_ids(self):
        """Ensure get_resource_ids works."""
        result = self.tip.get_resource_ids()
        self.assertEqual(['J:1234'], result)

    def test_is_provisioned(self):
        """Ensure is_provisioned works."""
        self.assertTrue(self.tip.is_provisioned(None))

    def test_get_provisioning_state(self):
        """Ensure get_provisioning_state works."""
        self.assertEqual((True, []), self.tip.get_provisioning_state(None))

    @mock.patch('provisioners.oneminutetip.LOGGER.warning', mock.Mock())
    @mock.patch('provisioners.oneminutetip.OneMinuteTip.provision_host')
    @mock.patch('provisioners.oneminutetip.OneMinuteTip.get_distro_requires')
    def test_provision(self, mock_get_distro, mock_provision):
        """Ensure provision doesn't crash."""
        mock_provision.return_value = ('1234', 'host1234')
        mock_get_distro.return_value = 'rhel8'
        self.tip.provision(**{'preprovision': ''})

        self.tip.provision(**{'preprovision': '0'})

        with self.assertRaises(ValueError):
            self.tip.provision(**{'preprovision': 'x,y'})

    def test_update_provisioning_request(self):
        """Ensure update_provisioning_request works."""
        with mock.patch.object(self.tip.rgs[0], 'preprovisioned', True):
            self.tip.update_provisioning_request(self.tip.rgs[0])

        self.tip.update_provisioning_request(self.tip.rgs[0])

        self.assertIn('<recipe id="123" job_id="1234"', self.tip.rgs[0].recipeset.restraint_xml)
