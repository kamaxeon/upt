"""Test cases for aws_setup module."""
import datetime
import json
import os
import unittest
from unittest.mock import mock_open
from unittest.mock import patch

from botocore.stub import Stubber

from plumbing.aws_setup import AWSSetup
from plumbing.aws_setup import LAUNCH_TEMPLATE_NAME
from plumbing.aws_setup import UPT_DEFAULT_KEYNAME
from tests.const import AWS_UPT_CONFIG

# pylint: disable=no-member, invalid-name


class FakeResource:
    """Fake ec2 resource for key pairs."""

    def __init__(self, keyname):
        """Create the object."""
        self.name = keyname


class FakeObj:
    """A fake object for mocking vpc, igws, sg, subnets and route tables."""

    def __init__(self, vpc_id):
        self.id = vpc_id

        self.called_igws = False
        self.called_sgs = False
        self.called_subnets = False
        self.called_rtas = False

        self.exp_tags = [{'Key': 'Name', 'Value': 'upt.default.vpc'},
                         {'Key': 'Name', 'Value': 'upt.default.igw'},
                         {'Key': 'Name', 'Value': 'upt.default.subnet'},
                         {'Key': 'Name', 'Value': 'upt.default.rt'},
                         {'Key': 'Name', 'Value': 'upt.default.sg'}]

    def all(self):
        """Fake all method."""
        return [self]

    @property
    def internet_gateways(self):
        """Fake internet_gateways property."""
        self.called_igws = True

        return self

    @property
    def security_groups(self):
        """Fake security_groups property."""
        self.called_sgs = True

        return self

    def create_tags(self, Tags=None):
        """Fake create_tags method."""
        assert_ok = False
        for i, _ in enumerate(self.exp_tags):
            if [self.exp_tags[i]] == Tags:
                assert_ok = True
                del self.exp_tags[i]
                break

        assert assert_ok, "Expected tags not created!"

    @property
    def subnets(self):
        """Fake subnets property."""
        self.called_subnets = True

        return self

    @property
    def route_tables(self):
        """Fake route_tables property."""
        self.called_rtas = True

        return self


class TestAWSSetup(unittest.TestCase):
    """Testcase for AWSSetup."""

    def setUp(self) -> None:
        """Prepare for testing."""
        self.aws_setup = AWSSetup()
        self.mocks = []

    @patch.dict(os.environ, {'AWS_UPT_CONFIG': AWS_UPT_CONFIG})
    def test_aws_config(self):
        """Ensure aws_config is taken from AWS_UPT_CONFIG env. variable and is json-decoded."""
        self.assertEqual(self.aws_setup.aws_config, json.loads(AWS_UPT_CONFIG))

    def test_create_default_vpc_with_tags(self):
        """Ensure create_default_vpc_with_tag works."""
        client_stub = Stubber(self.aws_setup.ec2_client)

        client_stub.add_response('create_default_vpc', {'Vpc': {'State': 'pending', 'VpcId': 'upt.default.vpc'}}, {})

        expected_params = {'GroupId': 'vpc-1234',
                           'IpPermissions': [{'FromPort': 22,
                                              'IpProtocol': 'tcp',
                                              'IpRanges': [{
                                                  'CidrIp': '0.0.0.0'}],
                                              'ToPort': 22}]}
        client_stub.add_response('authorize_security_group_ingress', {}, expected_params)

        # Ready, activate stubber.
        client_stub.activate()

        with patch.object(self.aws_setup.ec2_resource, 'Vpc') as mock_vpc:
            mock_obj = FakeObj('vpc-1234')
            mock_vpc.return_value = mock_obj
            # Call method under test.
            self.aws_setup.create_default_vpc_with_tags('0.0.0.0')

            self.assertTrue(all([mock_obj.called_igws, mock_obj.called_sgs, mock_obj.called_subnets,
                                 mock_obj.called_rtas]))

    def test_setup(self):
        """Ensure setup works."""
        client_stub = Stubber(self.aws_setup.ec2_client)

        client_stub.add_response('describe_vpcs', {}, {'Filters': self.aws_setup.vpc_filter})
        client_stub.add_response('describe_subnets', {}, {'Filters': [{'Name': 'vpc-id', 'Values': ['vpc-1234']}]})
        client_stub.activate()
        with patch.object(self.aws_setup, 'create_default_vpc_with_tags', new=lambda *args: (1, 'vpc-1234', 3)):
            with patch.object(self.aws_setup, 'create_template', new=lambda *args: None):
                with patch.object(self.aws_setup, 'does_default_keypair_exist', new=lambda *args: True):
                    self.aws_setup.setup('0.0.0.0', None, 't2.micro')

    def test_setup_alt_case(self):
        """Ensure setup can find existing vpc and calls method to create keyfile if needed."""

        client_stub = Stubber(self.aws_setup.ec2_client)

        client_stub.add_response('describe_vpcs', {'Vpcs': [{'VpcId': 'vpc-1234'}]},
                                 {'Filters': self.aws_setup.vpc_filter})
        client_stub.add_response('describe_subnets', {}, {'Filters': [{'Name': 'vpc-id', 'Values': ['vpc-1234']}]})
        client_stub.activate()
        with patch.object(self.aws_setup.ec2_resource, 'Vpc') as mock_vpc:
            mock_obj = FakeObj('vpc-1234')
            mock_vpc.return_value = mock_obj
            with patch.object(self.aws_setup, 'create_default_vpc_with_tags', new=lambda *args: (1, 'vpc-1234', 3)):
                with patch.object(self.aws_setup, 'create_template', new=lambda *args: None):
                    with patch.object(self.aws_setup, 'does_default_keypair_exist', new=lambda *args: False):
                        with patch.object(self.aws_setup, 'create_keypair', new=lambda *args: None):
                            self.aws_setup.setup('0.0.0.0', 'keyfile', 't2.micro')

    @patch('upt.logger.LOGGER.debug')
    def test_create_keypair(self, mock_debug):
        """Ensure create_keypair works."""
        client_stub = Stubber(self.aws_setup.ec2_client)

        client_stub.add_response('create_key_pair', {'KeyMaterial': 'deadbeef'}, {'KeyName': UPT_DEFAULT_KEYNAME})
        client_stub.activate()

        with patch('builtins.open', mock_open()) as mock_file:
            self.aws_setup.create_keypair('fake')
            mock_file.assert_called_with('fake', 'w', encoding='utf-8')

    @patch('upt.logger.LOGGER.info')
    def test_does_default_keypair_exist_found(self, mock_info):
        """Ensure does_default_keypair_exist works when there are key pairs."""

        # When correct key pair found.
        with patch.object(self.aws_setup, 'ec2_resource') as fh:
            fh.key_pairs.all.return_value = [FakeResource(UPT_DEFAULT_KEYNAME)]

            self.assertTrue(self.aws_setup.does_default_keypair_exist())

        mock_info.assert_called()

    @patch('upt.logger.LOGGER.info')
    def test_does_default_keypair_exist_not_found(self, mock_info):
        """Ensure does_default_keypair_exist works when no key pairs are found at all."""
        # When no key pairs found.
        with patch.object(self.aws_setup, 'ec2_resource') as fh:
            fh.key_pairs.all.return_value = []

            self.assertFalse(self.aws_setup.does_default_keypair_exist())

        mock_info.assert_called()

    @patch('upt.logger.LOGGER.info')
    def test_does_default_keypair_exist_mismatch(self, mock_info):
        """Ensure does_default_keypair_exist works when key pair name doesn't match."""
        with patch.object(self.aws_setup, 'ec2_resource') as fh:
            fh.key_pairs.all.return_value = [FakeResource('bad-keyname')]

            self.assertFalse(self.aws_setup.does_default_keypair_exist())

        mock_info.assert_called()

    @patch('upt.logger.LOGGER.info')
    def test_create_template(self, mock_info):
        """Ensure create_template works."""
        args = ('t2.micro', 1234, 'subnet-1234')

        retval = '2020-09-25T13:08:26.364232'
        with patch.object(self.aws_setup, 'get_next_token', return_value=retval):
            with patch.object(self.aws_setup.ec2_client, 'create_launch_template') as mock_create_launch_template:
                params = {'ClientToken': retval,
                          'LaunchTemplateName': LAUNCH_TEMPLATE_NAME,
                          'VersionDescription': '0.1',
                          'LaunchTemplateData': self.aws_setup.launch_template_data(*args)}
                self.aws_setup.create_template(*args)

                mock_info.assert_called()
                mock_create_launch_template.assert_called_with(**params)

    def test_get_next_token(self):
        """Ensure get_next_token works."""
        token = f'{datetime.datetime.utcnow().isoformat()}Z'
        part_token = token.split(':')[0]

        self.assertEqual(self.aws_setup.get_next_token().split(':')[0], part_token)
