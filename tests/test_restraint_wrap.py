"""Test cases for RestraintWrap __main__ module."""
import pathlib
import unittest
from unittest import mock

from click.testing import CliRunner

from plumbing.format import ProvisionData
from plumbing.objects import ResourceGroup
from restraint_wrap.__main__ import cli
from restraint_wrap.__main__ import main
from restraint_wrap.__main__ import test
from restraint_wrap.__main__ import wait_on_done
from upt.misc import RET

# pylint: disable=no-self-use


class TestRestraintWrap(unittest.TestCase):
    """Test cases for RestraintWrap __main__ module."""

    @mock.patch('builtins.print', mock.Mock())
    def test_test_api(self):
        # pylint: disable=no-self-use
        """Test that click api (test) executes."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            response = runner.invoke(cli, ['test', '--dump',
                                           '--rc', 'rcfile', '-i', 'ignore', '-o', 'tempdir'])
            self.assertEqual(response.exit_code, 1)

    @mock.patch('restraint_wrap.runner.Runner.run', lambda x: RET.PROVISIONING_PASSED.value)
    @mock.patch('restraint_wrap.__main__.wait_on_done')
    def test_test_api2(self, mock_wait):
        # pylint: disable=no-self-use
        """Test that click api (test) executes."""
        mock_prov = mock.Mock()
        mock_prov.rgs = []
        mock_wait.return_value = (1, [mock_prov])
        runner = CliRunner()

        with runner.isolated_filesystem():
            pathlib.Path('rcfile').touch()
            pathlib.Path('ignore').touch()

            response = runner.invoke(cli, ['test', '--rc', 'rcfile', '-i', 'ignore',
                                           '-o', 'tempdir', '--no-dump', '--no-upload'])
            self.assertEqual(response.exit_code, 0)

    @mock.patch('restraint_wrap.__main__.cli')
    def test_restraint_main_method(self, mock_cli):
        # pylint: disable=no-self-use
        """Ensure main works."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            main()

    def test_restraint_test_method(self):
        # pylint: disable=no-self-use
        """Ensure test requires --rc file when --dump is used."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            response = runner.invoke(test, ['--rc', 'non-existent', '--dump'])
            self.assertIn('please specify valid rc file', str(response.exception))

    @mock.patch('builtins.print')
    @mock.patch('restraint_wrap.__main__.time.sleep', mock.Mock())
    @mock.patch('restraint_wrap.__main__.ProvisionData.deserialize_file')
    def test_wait_on_done(self, mock_deserialize, mock_print):
        """Ensure wait_on_done works."""
        rg_ready = ResourceGroup()
        rg_ready.status = str(RET.PROVISIONING_PASSED)
        mock_deserialize.return_value = provs = ProvisionData()
        bkr = provs.get_provisioner('beaker', create=True)
        bkr.rgs = [rg_ready]

        def once(_):
            """Return True on 1st call, False on subsequent calls."""
            try:
                return once.x is True
            except AttributeError:
                once.x = False
                return True

        with mock.patch('restraint_wrap.__main__.any', once):
            wait_on_done('whatever')

        mock_print.assert_called_with('restraint test runner waiting on provisioners...')

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('restraint_wrap.__main__.time.sleep', mock.Mock())
    @mock.patch('restraint_wrap.__main__.ProvisionData.deserialize_file')
    def test_wait_on_done_err(self, mock_deserialize):
        """Ensure wait_on_done does not handle invalid internal data."""
        mock_deserialize.return_value = provs = ProvisionData()
        bkr = provs.get_provisioner('beaker', create=True)
        bkr.rgs = ['blah']

        with self.assertRaises(AttributeError):
            wait_on_done('whatever')

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('restraint_wrap.__main__.ProvisionData.deserialize_file')
    def test_wait_on_done_exit(self, mock_deserialize):
        """Ensure wait_on_done does sys.exit(1) if there are no resources."""
        mock_sec = mock.Mock()
        mock_sec.rgs = []
        mock_sec.provisioners = [mock_sec]

        mock_deserialize.side_effect = [mock_sec]

        with self.assertRaises(SystemExit):
            wait_on_done('whatever')
