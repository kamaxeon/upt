"""Test cases for task_result module."""
import pathlib
import unittest

from bs4 import BeautifulSoup as BS
from click.testing import CliRunner
from freezegun import freeze_time

from plumbing.objects import Host
from restraint_wrap.task_result import TaskResult


class TestTaskResult(unittest.TestCase):
    """Test cases for task_result module."""

    @freeze_time('2020-11-13T16:34:45.912589Z', tz_offset=1)
    def setUp(self) -> None:
        # pylint: disable=protected-access
        self.tasks = [BS('<task id="1" name="a3" status="Completed" result="Pass"> <fetch url="git://"/>'
                         '<params> <param name="CKI_MAINTAINERS" '
                         'value="abc de &lt;abcde@redhat.com&gt; / abcde, aaa aaaaaaaa &lt;aaaaaaaa'
                         '@redhat.com&gt; / aaaaaaaa, mmmmmm mmmmmmmm &lt;mmmmm@redhat.com&gt;"/> '
                         '<param name="CKI_NAME" value="a3"/> </params></task>',
                         'xml').find('task')]
        self.host = Host({'hostname': 'hostname1', 'recipe_id': 1234})
        self.task_result = TaskResult(self.host, self.tasks[0], 1, 'Pass', 'Completed')
        self.host._task_results.append(self.task_result)

    def test_test_maintainers(self):
        """Ensure TaskResult is created OK and test_maintainers are parsed."""
        self.assertEqual(self.task_result.recipe_id, 1234)
        expected = [{'name': 'abc de', 'email': 'abcde@redhat.com', 'gitlab': 'abcde'},
                    {'name': 'aaa aaaaaaaa', 'email': 'aaaaaaaa@redhat.com', 'gitlab': 'aaaaaaaa'},
                    {'name': 'mmmmmm mmmmmmmm', 'email': 'mmmmm@redhat.com', 'gitlab': ''}]
        self.assertEqual(expected, self.task_result.test_maintainers)

    def test_output_files_from_path(self):
        """Ensure output_files_from_path can really find files in expected directories."""
        runner = CliRunner(mix_stderr=False)

        task_result = TaskResult(self.host, self.tasks[0], 1, 'Pass', 'Completed')
        with runner.isolated_filesystem():
            path_prefix = 'run/run.done'
            pathlib.Path(path_prefix).mkdir(exist_ok=True, parents=True)
            full_path = pathlib.Path(path_prefix, f'recipes/{task_result.recipe_id}/tasks/{task_result.task_id}')
            full_path.mkdir(exist_ok=True, parents=True)
            pathlib.Path(full_path, 'file1').touch()
            pathlib.Path(full_path, 'file2').touch()

            result = task_result.output_files_from_path(path_prefix)
            self.assertCountEqual(
                result,
                [{'name': 'file1', 'path': str(full_path)}, {'name': 'file2', 'path': str(full_path)}])

    def test_get_most_recent_result(self):
        """Ensure get_most_recent_result works."""
        list = [TaskResult(self.host, self.tasks[0], 1, 'Pass', 'Completed'),
                TaskResult(self.host, self.tasks[0], 2, 'Running', ''),
                TaskResult(self.host, self.tasks[0], 2, 'FAIL', 'Completed'),
                TaskResult(self.host, self.tasks[0], 4, 'FAIL', 'Completed')]

        result = TaskResult.get_most_recent_result(self.host.recipe_id, 2, list)
        self.assertEqual(list[2], result)

        result = TaskResult.get_most_recent_result(self.host.recipe_id, 333, list)
        self.assertIsNone(result)
